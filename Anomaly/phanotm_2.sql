-- Phantom | Insert new record | Connection 2
USE task3
GO

BEGIN TRANSACTION

INSERT INTO client (id, name)
VALUES (10, 'NEW CLIENT')

COMMIT TRANSACTION

-- Change back
DELETE FROM client
WHERE id = 10