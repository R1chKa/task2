-- Phantom | Read data twice | Connection 1
USE task3
GO

BEGIN TRANSACTION

SELECT *
FROM client 
WHERE id > 3

WAITFOR DELAY '00:00:05'

SELECT *
FROM client 
WHERE id > 3

COMMIT TRANSACTION