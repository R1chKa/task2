DROP DATABASE IF EXISTS task3
GO

CREATE DATABASE task3
GO

USE task3
GO

CREATE TABLE client (
	id integer NOT NULL,
	name varchar(20) NULL,
	surname varchar(20) NULL,
	age integer NULL,
	city varchar(20) NULL
)
GO

CREATE TABLE wish (
	id integer NOT NULL,
	product varchar(20) NULL,
	description varchar(20) NULL,
	quantity integer NULL
)
GO

--  Inserting data into table client
INSERT INTO client (id, name, surname, age, city) 
VALUES (1, 'Bob', 'Fox', 22, 'London');
INSERT INTO client (id, name, surname, age, city)
VALUES (2, 'David', 'Ericson', 23, 'Paris');
INSERT INTO client (id, name, surname, age, city)
VALUES (3, 'Dolores', 'Walters', 19, 'Lviv');
INSERT INTO client (id, name, surname, age, city)
VALUES (4, 'Lex', 'Barret', 27, 'Budapest');
INSERT INTO client (id, name, surname, age, city)
VALUES (5, 'Ami', 'Derrickson', 32, 'Bangkok');
INSERT INTO client (id, name, surname, age, city)
VALUES (6, 'Janette', 'Cheshire', 28, 'Tokio');

--  Inserting data into table wish
INSERT INTO wish (id, product, description, quantity) 
VALUES (1, 'Bike', 'sport', 2);
INSERT INTO wish (id, product, description, quantity) 
VALUES (2, 'Jeep', '4x4', 1);
INSERT INTO wish (id, product, description, quantity) 
VALUES (3, 'Sushi', 'japaniese', 10);
INSERT INTO wish (id, product, description, quantity) 
VALUES (4, 'Notebook', 'Acer', 1);
INSERT INTO wish (id, product, description, quantity) 
VALUES (5, 'T-shirt', '', 5);

SELECT * FROM client
GO

SELECT * FROM wish
GO