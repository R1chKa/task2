-- Dirty read | Update with rollback | Connection 1
USE task3
GO

BEGIN TRANSACTION

UPDATE client
SET name = 'UPDATED_BOB'
WHERE id = 1

WAITFOR DELAY '00:00:05'

ROLLBACK TRANSACTION
