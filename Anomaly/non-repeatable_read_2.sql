-- Non-repeatable read | Update data | Connection 2
USE task3
GO

BEGIN TRANSACTION

update client
set name = 'BOB_UPDATED' 
where id = 1

COMMIT TRANSACTION

-- Change back
UPDATE client
SET name = 'Bob' 
WHERE id = 1