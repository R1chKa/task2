-- Non-repeatable read | Read data twice | Connection 1
USE task3
GO

BEGIN TRANSACTION

SELECT * FROM client

WAITFOR DELAY '00:00:05'

SELECT * FROM client

COMMIT TRANSACTION